//
//  Todo+CoreDataProperties.h
//  CoreDataDemo
//
//  Created by James Cash on 22-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Todo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Todo (CoreDataProperties)

+ (NSFetchRequest<Todo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *task;
@property (nullable, nonatomic, copy) NSDate *created;

@end

NS_ASSUME_NONNULL_END
