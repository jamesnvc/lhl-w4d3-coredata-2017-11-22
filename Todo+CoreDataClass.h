//
//  Todo+CoreDataClass.h
//  CoreDataDemo
//
//  Created by James Cash on 22-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Todo : NSManagedObject

- (NSString*)dayCreated;

@end

NS_ASSUME_NONNULL_END

#import "Todo+CoreDataProperties.h"
