//
//  Todo+CoreDataClass.m
//  CoreDataDemo
//
//  Created by James Cash on 22-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Todo+CoreDataClass.h"

@implementation Todo

- (NSString*)dayCreated
{
    NSDateFormatter *fmtr = [[NSDateFormatter alloc] init];
    fmtr.timeStyle = NSDateFormatterNoStyle;
    fmtr.dateStyle = NSDateFormatterShortStyle;
    return [fmtr stringFromDate:self.created];
}

@end
