//
//  ViewController.m
//  CoreDataDemo
//
//  Created by James Cash on 22-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "Todo+CoreDataProperties.h"

@interface ViewController () <UITableViewDataSource,NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong) NSFetchedResultsController *fetchController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;

    NSFetchRequest *fetchReq = [Todo fetchRequest];
    fetchReq.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES] ];

    self.fetchController = [[NSFetchedResultsController alloc]
                            initWithFetchRequest:fetchReq
                            managedObjectContext:appDel.persistentContainer.viewContext
                            sectionNameKeyPath:@"dayCreated"
                            cacheName:nil];
    self.fetchController.delegate = self;
    NSError *err = nil;
    if (![self.fetchController performFetch:&err] || err != nil) {
        NSLog(@"Error fetching: %@", err.localizedDescription);
        abort();
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)printSaved
{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;

    NSFetchRequest *fetchReq = [Todo fetchRequest];
    fetchReq.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES] ];
    NSError *err = nil;
    NSArray<Todo*>* todos = [appDel.persistentContainer.viewContext
                             executeFetchRequest:fetchReq
                             error:&err];
    if (err != nil) {
        NSLog(@"Couldn't search %@", err.localizedDescription);
        abort();
    }
    NSLog(@"results: %@", todos);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchController.sections[section].numberOfObjects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TodoCell"];

    UILabel *cellLabel = (UILabel*)[cell viewWithTag:1];

    Todo* todo = [self.fetchController objectAtIndexPath:indexPath];
    cellLabel.text = todo.task;

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.fetchController.sections[section].name;
}

/*
 Assume self has a property 'tableView' -- as is the case for an instance of a UITableViewController
 subclass -- and a method configureCell:atIndexPath: which updates the contents of a given cell
 with information from a managed object at the given index path in the fetched results controller.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
//                    atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
