//
//  AddTodoViewController.m
//  CoreDataDemo
//
//  Created by James Cash on 22-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "AppDelegate.h"
#import "AddTodoViewController.h"
#import "Todo+CoreDataProperties.h"

@interface AddTodoViewController ()
@property (strong, nonatomic) IBOutlet UITextField *taskField;

@end

@implementation AddTodoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveTodo:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;

    Todo *newTodo = [[Todo alloc] initWithContext:appDel.persistentContainer.viewContext];
    newTodo.task = self.taskField.text;
    newTodo.created = [[NSDate alloc] init];

    [appDel saveContext];

    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

@end
